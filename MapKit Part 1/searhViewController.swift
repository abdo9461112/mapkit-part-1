//
//  searhViewController.swift
//  MapKit Part 1
//
//  Created by abdo emad  on 16/08/2023.
//

import UIKit
import MapKit
import CoreLocation
class searhViewController: UIViewController , UITextFieldDelegate, CLLocationManagerDelegate, MKMapViewDelegate{
    
    var locationManager = CLLocationManager()
    
    @IBOutlet weak var mapVeiw: MKMapView!
    @IBOutlet weak var textdstination: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textdstination.delegate = self
        mapVeiw.delegate = self
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyKilometer
        locationManager.requestWhenInUseAuthorization()
        
    }
    
    @IBAction func segmentedControl(_ sender: UISegmentedControl) {
        
        switch sender.selectedSegmentIndex {
        case 0:
            mapVeiw.mapType = .standard
        case 1:
            mapVeiw.mapType = .hybrid
        default:
            break
        }
    }
    
    
    @IBAction func btnSearch(_ sender: Any) {
        let destination = textdstination.text!
        if destination != "" {
            searhForDestination(destination)
        }else{
            showAlert(msg: "please enter the destination")
            
        }
        
        
    }
    
    @IBAction func getDirection(_ sender: UIButton) {
        if let userloaction = locationManager.location{
            drewDirection(startingloc: userloaction.coordinate, endingLoc: mapVeiw.centerCoordinate)
        }
    }
    func drewDirection(startingloc : CLLocationCoordinate2D, endingLoc : CLLocationCoordinate2D ){
         
        let startingLoc = MKPlacemark(coordinate: startingloc)
        let endingLoc = MKPlacemark(coordinate: endingLoc)
        
        let startingItem = MKMapItem(placemark: startingLoc)
        let endingItem = MKMapItem(placemark: endingLoc)
        
        let reqest = MKDirections.Request()
        reqest.source = startingItem
        reqest.destination = endingItem
        reqest.transportType = .automobile
        reqest.requestsAlternateRoutes = true
        
        let direction = MKDirections(request: reqest)
        direction.calculate { response, error in
            guard let response = response else {
                if let error = error {
                    print(error.localizedDescription)
                }
                return
            }
           
            for route in response.routes {
                self.mapVeiw.addOverlay(route.polyline)
                self.mapVeiw.setVisibleMapRect(route.polyline.boundingMapRect, animated: true)
            }
        }
    }
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let render = MKPolylineRenderer(overlay: overlay)
        render.strokeColor = .tintColor
    
        
        return render
    }
    func searhForDestination(_ destination : String){
        let gocoder = CLGeocoder()
        gocoder.geocodeAddressString(destination) { places, error in
            
            guard let place = places?.first , error == nil else{
                self.showAlert(msg: "there is no data to display ")
                return
            }
            guard let location = place.location else {return}
            self.textdstination.text = ""
            self.addPin(in: place, at: location)
            
        }
        
        
    }
    
    @IBAction func locationButton(_ sender: UIButton) {
        locationManager.requestLocation()
        checkAuthorization()

    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last{
            zoomToUserLocation(location)
        }
    }
    func zoomToUserLocation(_ location : CLLocation){
//        let region = MKCoordinateRegion(center: location.coordinate, latitudinalMeters: 1000, longitudinalMeters: 1000)
        let fixedSpan = 0.05
            
            // Create a region with the user's location as the center and the fixed span
            let span = MKCoordinateSpan(latitudeDelta: fixedSpan, longitudeDelta: fixedSpan)
            let region = MKCoordinateRegion(center: location.coordinate, span: span)
            
            // Set the region on the map view
            mapVeiw.setRegion(region, animated: true)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("error with the Location\(error.localizedDescription)")
    }

    
    func addPin(in place : CLPlacemark  , at location :CLLocation  ){
        
        let pin = MKPointAnnotation()
        pin.coordinate = location.coordinate
        pin.title = "\(place.name ?? "there is no data to display ")"
        pin.subtitle = "\(place.locality ?? "there is no data to display " )"
        
        mapVeiw.addAnnotation(pin)
        view.endEditing(true)
        
        
        let region = MKCoordinateRegion(center: location.coordinate, latitudinalMeters: 2000, longitudinalMeters: 2000)
        mapVeiw.setRegion(region, animated: true)
        
    }
    
    func checkAuthorization(){
        switch locationManager.authorizationStatus {
        case .notDetermined:
            locationManager.requestAlwaysAuthorization()
            break
        case .authorizedWhenInUse:
            locationManager.requestAlwaysAuthorization()
            mapVeiw.showsUserLocation = true
            break
        case .authorizedAlways:
            locationManager.startUpdatingLocation()
            mapVeiw.showsUserLocation = true
            break
        case .denied:
            showAlert(msg: " please access to location ")
            break
        case .restricted:
            showAlert(msg: " please access to location ")
            break
        default:
            print ("abdo")
            break
        }
    }
    
    func showAlert(msg: String){
        let alert = UIAlertController(title: "alert", message: msg, preferredStyle:.alert)
        alert.addAction(UIAlertAction(title: "Close", style: .default))
        present(alert, animated: true, completion: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == textdstination {
            btnSearch((Any).self)
        }
        return true
        
        
    }
}
