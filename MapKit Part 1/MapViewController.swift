//
//  MapViewController.swift
//  MapKit Part 1
//
//  Created by abdo emad  on 14/08/2023.
//

import UIKit
import MapKit

class MapViewController: UIViewController, CLLocationManagerDelegate {
    
    var locationManger = CLLocationManager()
    
    @IBOutlet weak var mapView: MKMapView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManger.delegate = self
        locationManger.desiredAccuracy = kCLLocationAccuracyKilometer
        locationManger.allowsBackgroundLocationUpdates = true
        
        if islocationSefviceEnabled(){
            checkAuthorization()
        }else{
            showAlert(msg: "please enable location service")
        }
    }
    
    
    func islocationSefviceEnabled() -> Bool{
        return CLLocationManager.locationServicesEnabled()
        
    }
    func checkAuthorization(){
        switch locationManger.authorizationStatus {
        case .notDetermined:
            locationManger.requestAlwaysAuthorization()
            break
        case .authorizedWhenInUse:
            locationManger.requestAlwaysAuthorization()
            locationManger.startUpdatingLocation()
            mapView.showsUserLocation = true
            break
        case .authorizedAlways:
            locationManger.startUpdatingLocation()
            mapView.showsUserLocation = true
            break
        case .denied:
            showAlert(msg: " please access to location ")
            break
        case .restricted:
            showAlert(msg: " please access to location ")
            break
        default:
            print ("abdo")
            break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last{
            print("loction: \(location.coordinate) ")
            zoomtouserLocation(location: location)
        }
        locationManger.stopUpdatingLocation()
    }
    func zoomtouserLocation(location : CLLocation){
        
        let region = MKCoordinateRegion(center: location.coordinate, latitudinalMeters: 500, longitudinalMeters: 500)

        mapView.setRegion(region, animated: true)
    }
    func showAlert(msg : String){
        let alert = UIAlertController(title: "Alert", message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "close", style:.default))
        alert.addAction(UIAlertAction(title: "Sittings", style: .default, handler: {action in
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)}))
        
        present(alert,animated: true,completion: nil)
    }
    
    
    
    
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            locationManger.requestAlwaysAuthorization()
            break
        case .authorizedWhenInUse:
            locationManger.requestAlwaysAuthorization()
            locationManger.startUpdatingLocation()
            mapView.showsUserLocation = true
            break
        case .authorizedAlways:
            locationManger.startUpdatingLocation()
            mapView.showsUserLocation = true
            break
        case .denied:
            showAlert(msg: " please access to location ")
            break
        default:
            print ("abdo")
            break
        }
    }
}
