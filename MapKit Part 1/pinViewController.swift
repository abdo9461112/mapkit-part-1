//
//  pinViewController.swift
//  MapKit Part 1
//
//  Created by abdo emad  on 20/08/2023.
//

import UIKit
import MapKit
class pinViewController: UIViewController, MKMapViewDelegate {
    
    @IBOutlet weak var mapView: MKMapView!
    var prevlociton : CLLocation? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView.delegate = self
        
    }
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        let newloction = (CLLocation(latitude: mapView.centerCoordinate.latitude, longitude: mapView.centerCoordinate.longitude))
        if prevlociton == nil || prevlociton!.distance(from: newloction) > 50{
            getLoctionInfo(Loction: newloction)
        }
        
        func getLoctionInfo(Loction : CLLocation){
            prevlociton = Loction
            let geCoder = CLGeocoder()
            geCoder.reverseGeocodeLocation(Loction) { places, error in
                guard let place = places?.first, error == nil else {return}
                
                print("-------")
                print("place name \(place.name ?? "no data to display ")")
                print("place country \(place.country ?? "no data to display ")")
                print("place country code \(place.isoCountryCode ?? "no data to display ")")
                print("place administrativeArea \(place.administrativeArea ?? "no data to display ")")
                print("place locality \(place.locality ?? "no data to display ")")
                print("place postalcode \(place.postalCode ?? "no data to display ")")
                
                
            }
            
        }
    }
}
