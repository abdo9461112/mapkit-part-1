//
//  ViewController.swift
//  MapKit Part 1
//
//  Created by abdo emad  on 14/08/2023.
//

import UIKit
import MapKit

class ViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    override func viewDidLoad() {
        super.viewDidLoad()

        let intiilaLoc = CLLocation(latitude: 30.88799, longitude: 31.45033)
        setSatartingLoction(loction: intiilaLoc, destince: 5000)
        addAnnotation()
    }

    func setSatartingLoction(loction: CLLocation, destince : CLLocationDistance){
        let region = MKCoordinateRegion(center: loction.coordinate, latitudinalMeters: destince, longitudinalMeters: destince )
        mapView.setRegion(region, animated: true)
        mapView.setCameraBoundary(MKMapView.CameraBoundary(coordinateRegion: region), animated: true)
        let zoomrange = MKMapView.CameraZoomRange(maxCenterCoordinateDistance: 100000
        )
        mapView.setCameraZoomRange(zoomrange, animated: true)
        
        
    }
    func addAnnotation(){
        let pin = MKPointAnnotation()
        pin.coordinate = CLLocationCoordinate2D(latitude: 30.88799, longitude: 31.45033)
        pin.title = "My place"
        pin.subtitle = " This is my place"
        mapView.addAnnotation(pin)
    }
}

